#!/usr/bin/python3

import pytest
from random import randint

@pytest.fixture(scope="function", autouse=True)
def isolate(fn_isolation):
    # perform a chain rewind after completing each test, to ensure proper isolation
    # https://eth-brownie.readthedocs.io/en/v1.10.3/tests-pytest-intro.html#isolation-fixtures
    pass


@pytest.fixture(scope="module")
def token(Token, accounts, initial_supply=randint(1e21, 1e22)):
    return Token.deploy("Test Token", "TST", 18, initial_supply, accounts[3], {'from': accounts[0]})


@pytest.fixture(scope="module")
def shibaku_coins(ShibakuCoins, accounts, token):
    return ShibakuCoins.deploy(accounts[3], token, randint(1e5, 1e7), {'from': accounts[0]})
