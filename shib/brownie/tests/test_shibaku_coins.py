#!/usr/bin/python3

import pytest
import brownie

def test_set_management(shibaku_coins, accounts):
    assert (shibaku_coins.management()) == accounts[3]
    shibaku_coins.setManagement(accounts[1], {'from': accounts[3]})
    assert (shibaku_coins.management()) == accounts[1]


def test_flip_coins(token, shibaku_coins, accounts):
    coins = shibaku_coins.getCoins()
    assert all(coins)
    coin_nums = [1, 3, 5, 7, 9, 11]
    with brownie.reverts('Insufficient allowance'):
        shibaku_coins.flipCoins(coin_nums, [False, False, False, False, False, False])

    supply = token.totalSupply()
    token.approve(shibaku_coins, supply, {'from': accounts[0]})
    balance_before = token.balanceOf(accounts[0])
    shibaku_coins.flipCoins(coin_nums, [False, False, False, False, False, False])
    balance_after = token.balanceOf(accounts[0])
    price = shibaku_coins.flipPrice()
    assert balance_before - balance_after == len(coin_nums) * price
    coins = shibaku_coins.getCoins()
    for i, coin in enumerate(coins):
        if i+1 in coin_nums:
            assert(coin) == False
        else:
            assert(coin) == True

