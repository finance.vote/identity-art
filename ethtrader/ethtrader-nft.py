from PIL import Image, ImageDraw, ImageFont
from glob import glob
import random

templates = sorted(glob('./img/templates/*'))
colors = ['black', 'black', 'black', 'white', 'black', 'white', 'white', 'white', 'black']
frequencies = [7 * (2 ** (len(templates) - 1 - i)) for i in range(len(templates))]
print(templates)
print(frequencies)
print(sum(frequencies))
ids = []

for i, f in enumerate(frequencies):
    ids.extend(f * [i])

random.shuffle(ids)
print(ids)


def create_img(id_num, template_index):
    img = Image.open(templates[template_index])
    draw = ImageDraw.Draw(img)
    # position1 = [img.size[0] / 2, img.size[1] / 2]
    position1 = [img.size[0] * 0.875, img.size[1] * 0.465]
    font1 = ImageFont.truetype('../fonts/FrankNew.otf', 126)
    message1 = str(id_num)
    w1, h1 = draw.textsize(message1, font=font1)
    position1[0] = position1[0] - (w1/2)
    # position1[1] = position1[1] - (3 * h1/2)

    if colors[template_index] == 'white':
        color = (255, 255, 255)
    else:
        color = (0, 0, 0)

    draw.text(position1, message1, fill=color, font=font1, align='center')

    # img.show()
    img.save('./img/generated/voter-{}.png'.format(message1))

for i, template_index in enumerate(ids):
    print(i)
    create_img(i+1, template_index)

