# FROM r-base
# FROM python
# FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7
# RUN apt install python
# COPY ./requirements.txt /var/www/requirements.txt
# RUN pip install -r /var/www/requirements.txt

From r-base
RUN apt-get update && apt-get install -y python3-pip build-essential libcurl4-gnutls-dev libxml2-dev libssl-dev


COPY . /usr/local/src/myscripts
WORKDIR /usr/local/src/myscripts
RUN python3 -m pip install -r requirements.txt

# Expose the required port
# EXPOSE 5000

CMD ["./run_flask.sh"]
